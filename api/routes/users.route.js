const {Router} = require("express");
const {postUsersValid, getUserValid, putUserValid} = require("../middlewares/validation.middleware");
const {userAuthorisation} = require("../middlewares/authorisation.middleware")
const UserService = require("../../services/user.service");


const router = Router();

router.post("/", postUsersValid, async (req, res) => {
    try {
        await UserService.post(req, res);
    } catch (err) {
        res.status(500).send("Internal Server Error");
    }
});

router.get("/:id", getUserValid, async (req, res) => {
    try {
        await UserService.get(req, res);
    } catch (err) {
        res.status(500).send("Internal Server Error");
    }
});

router.put("/:id", userAuthorisation, putUserValid, (req, res) => {
    try {
        UserService.put(req, res);
    } catch (err) {
        res.status(500).send("Internal Server Error");
    }
});

module.exports = router;