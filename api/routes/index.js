const usersRoute = require("./users.route");
const healthRoute = require("./health.route");
const transactionsRoute = require("./transactions.route");
const eventsRoute = require("./events.route");
const betsRoute = require("./bets.route");
const statsRoute = require("./stats.route");

module.exports = app => {
    app.use("/users", usersRoute);
    app.use("/health", healthRoute);
    app.use("/transactions", transactionsRoute);
    app.use("/events", eventsRoute);
    app.use("/bets", betsRoute);
    app.use("/stats", statsRoute);
}