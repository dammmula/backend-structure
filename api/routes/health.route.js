const {Router} = require("express");

const router = Router();

router.get("/health", (req, res) => {
    try {
        res.send("Hello World!");
    } catch (error) {
        res.status(500).send("Internal Server Error");
    }
});

module.exports = router;