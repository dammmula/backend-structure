const {Router} = require("express");
const {postTransactionsValid} = require("../middlewares/validation.middleware");
const {adminAuthorisation} = require("../middlewares/authorisation.middleware");
const transactionService = require("../../services/transaction.service");
const router = Router();

router.post("/", postTransactionsValid, adminAuthorisation, async (req, res, next) => {
    try {
        await transactionService.post(req, res);
    } catch (error) {
        res.status(500).send("Internal Server Error");
    }
});

module.exports = router;