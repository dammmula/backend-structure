const jwt = require("jsonwebtoken");
const {stats} = require("../../config/stat");
const {Router} = require("express");
const {adminAuthorisation} = require("../middlewares/authorisation.middleware");

const router = Router();

router.get("/", adminAuthorisation, (req, res) => {
    try {
        res.send(stats);
    } catch (err) {
        res.status(500).send("Internal Server Error");
    }
});


module.exports = router;