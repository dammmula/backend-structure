const {Router} = require("express");
const {putEventValid} = require("../middlewares/validation.middleware");
const {adminAuthorisation} = require("../middlewares/authorisation.middleware");
const {postEventsValid} = require("../middlewares/validation.middleware");
const EventsService = require("../../services/events.service");

const router = Router();

router.post("/", postEventsValid, adminAuthorisation, (req, res) => {
    try {
        EventsService.post(req, res);
    } catch (err) {
        res.status(500).send("Internal Server Error");
    }
});

router.put("/:id", putEventValid, adminAuthorisation, (req, res) => {
    try {
        EventsService.put(req, res);
    } catch (err) {
        res.status(500).send("Internal Server Error");
    }
});

module.exports = router;