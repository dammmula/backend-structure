const {Router} = require("express");
const {postBetsAuthorisation} = require("../middlewares/authorisation.middleware");
const {postBetsValid} = require("../middlewares/validation.middleware");
const BetsService = require("../../services/bets.service");

const router = Router();

router.post("/", postBetsValid, postBetsAuthorisation, async (req, res) => {
    try {
        await BetsService.post(req, res);
    } catch (err) {
        res.status(500).send("Internal Server Error");
    }
});


module.exports = router;