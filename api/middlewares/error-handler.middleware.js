const errorHandlerMiddleware = (err, req, res, next) => {
    if (res.headersSent) {
        next(err);
    } else {
        res.status(500).send("Internal Server Error");
    }
}

module.exports = errorHandlerMiddleware;