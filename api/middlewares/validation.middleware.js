const joi = require("joi");
const jwt = require("jsonwebtoken");

const validationMiddleware = (schema, req, res, next) => {
    const isValidResult = schema.validate(req);

    if(isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
    } else {
        next();
    }
}


const postUsersValid = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
        type: joi.string().required(),
        email: joi.string().email().required(),
        phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/).required(),
        name: joi.string().required(),
        city: joi.string(),
    }).required();

    validationMiddleware(schema, req.body, res, next);
}

const getUserValid = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
    }).required();

    validationMiddleware(schema, req.params, res, next);
}

const putUserValid = (req, res, next) => {
    const schema = joi.object({
        email: joi.string().email(),
        phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
        name: joi.string(),
        city: joi.string(),
    }).required();

    validationMiddleware(schema, req.body, res, next);
}

const postTransactionsValid = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
        userId: joi.string().uuid().required(),
        cardNumber: joi.string().required(),
        amount: joi.number().min(0).required(),
    }).required();

    validationMiddleware(schema, req.body, res, next);
}

const postEventsValid = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
        type: joi.string().required(),
        homeTeam: joi.string().required(),
        awayTeam: joi.string().required(),
        startAt: joi.date().required(),
        odds: joi.object({
            homeWin: joi.number().min(1.01).required(),
            awayWin: joi.number().min(1.01).required(),
            draw: joi.number().min(1.01).required(),
        }).required(),
    }).required();

    validationMiddleware(schema, req.body, res, next);
}

const putEventValid = (req, res, next) => {
    const schema = joi.object({
        score: joi.string().required(),
    }).required();

    validationMiddleware(schema, req.body, res, next);
}

const postBetsValid = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
        eventId: joi.string().uuid().required(),
        betAmount: joi.number().min(1).required(),
        prediction: joi.string().valid('w1', 'w2', 'x').required(),
    }).required();

    validationMiddleware(schema, req.body, res, next);
}

module.exports = {
    postUsersValid,
    getUserValid,
    putUserValid,
    postTransactionsValid,
    postEventsValid,
    putEventValid,
    postBetsValid
}