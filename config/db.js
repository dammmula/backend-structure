const knex = require("knex");
const dbConfig = require("../knexfile");

let db;

try {
    db = knex(dbConfig.development);
    db.raw('select 1+1 as result')
} catch (error) {
    throw new Error('No db connection');
}

module.exports = db;