const BaseRepository = require("./base.repository");

class TransactionRepository extends BaseRepository {
    constructor() {
        super('transaction');
    }
}

class EventRepository extends BaseRepository {
    constructor() {
        super('event');
    }
}

class OddsRepository extends BaseRepository {
    constructor() {
        super('odds');
    }
}


class UserRepository extends BaseRepository {
    constructor() {
        super('user');
    }
}

class BetRepository extends BaseRepository{
    constructor() {
        super('bet');
    }
}

module.exports = {
    UserRepository: new UserRepository(),
    OddsRepository: new OddsRepository(),
    EventRepository: new EventRepository(),
    TransactionRepository: new TransactionRepository(),
    BetRepository: new BetRepository()
};