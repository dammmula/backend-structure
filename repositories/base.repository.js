const db = require("../config/db");

class BaseRepository {
    constructor(collectionName) {
        this.collectionName = collectionName;
    }

    async filter(obj) {
        const entries = Object.entries(obj);
        return await db(this.collectionName)
            .where(entries[0][0], entries[0][1])
            .andWhere(entries[1][0], entries[1][1]);
    }

    async getById(id) {
        return await db(this.collectionName).where('id', id);
    }

    async update(data, id) {
        const [result] = await db(this.collectionName)
            .where('id', id)
            .update(data).returning('*');
        return result;
    }

    async updateField(field, value, id) {
        return await db(this.collectionName)
            .where('id', id)
            .update(field, value).returning('*');
    }

    async insert(data) {
        const [result] = await db(this.collectionName).insert(data).returning('*');
        return result;
    }
}

module.exports = BaseRepository;