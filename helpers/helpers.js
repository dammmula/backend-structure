const jwt = require("jsonwebtoken");


const rewriteKeysHelper = (obj, arr) => {
    arr.forEach(key => {
        const index = key.indexOf('_');
        let newKey = key.replace('_', '');
        newKey = newKey.split('')
        newKey[index] = newKey[index].toUpperCase();
        newKey = newKey.join('');
        obj[newKey] = obj[key];
        delete obj[key];
    });
}

const getAccessToken = (result) => {
    return jwt.sign({ id: result.id, type: result.type }, process.env.JWT_SECRET);
}


module.exports = {
    rewriteKeysHelper,
    getAccessToken
}