const {UserRepository} = require("../repositories/repositories");
const {BetRepository} = require("../repositories/repositories");
const {EventRepository} = require("../repositories/repositories");
const {OddsRepository} = require("../repositories/repositories");
const {statEmitter} = require("../config/stat");
const {rewriteKeysHelper} = require("../helpers/helpers");

class EventService {
    async post(req, res) {
        req.body.odds.home_win = req.body.odds.homeWin;
        delete req.body.odds.homeWin;
        req.body.odds.away_win = req.body.odds.awayWin;
        delete req.body.odds.awayWin;
        const odds = await OddsRepository.insert(req.body.odds);
        delete req.body.odds;
        req.body.away_team = req.body.awayTeam;
        req.body.home_team = req.body.homeTeam;
        req.body.start_at = req.body.startAt;
        delete req.body.awayTeam;
        delete req.body.homeTeam;
        delete req.body.startAt;
        const event = await EventRepository.insert({
            ...req.body,
            odds_id: odds.id
        });
        statEmitter.emit('newEvent');
        const eventKeys = ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at'];
        rewriteKeysHelper(event, eventKeys);
        const oddsKeys = ['home_win', 'away_win', 'created_at', 'updated_at'];
        rewriteKeysHelper(odds, oddsKeys)
        return res.send({
            ...event,
            odds,
        });
    }

    async put(req, res) {
        const eventId = req.params.id;

        const bets = await BetRepository.filter({
            event_id: eventId,
            win: null
        })
        const [w1, w2] = req.body.score.split(":");
        let result;
        if(+w1 > +w2) {
            result = 'w1'
        } else if(+w2 > +w1) {
            result = 'w2';
        } else {
            result = 'x';
        }
        const event = await EventRepository.update({ score: req.body.score }, eventId);
        Promise.all(bets.map(async (bet) => {
            if(bet.prediction === result) {
                await BetRepository.update({
                    win: true
                }, bet.id);

                const [user] = await UserRepository.getById(bet.user_id);
                return await UserRepository.update({
                    balance: user.balance + (bet.bet_amount * bet.multiplier),
                }, bet.user_id);
            } else {
                return await BetRepository.update({
                    win: false
                }, bet.id);
            }
        }));
        setTimeout(() => {
            const eventKeys = ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at'];
            rewriteKeysHelper(event, eventKeys);
            res.send(event);
        }, 1000)

    }
}

module.exports = new EventService();