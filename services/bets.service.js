const {UserRepository} = require("../repositories/repositories");
const {BetRepository} = require("../repositories/repositories");
const {OddsRepository} = require("../repositories/repositories");
const {EventRepository} = require("../repositories/repositories");
const {statEmitter} = require("../config/stat");
const {rewriteKeysHelper} = require("../helpers/helpers");

class BetsService {
    async post (req, res) {
        let userId = req.userId;

        req.body.event_id = req.body.eventId;
        req.body.bet_amount = req.body.betAmount;
        delete req.body.eventId;
        delete req.body.betAmount;
        req.body.user_id = userId;

        const [user] = await UserRepository.getById(userId);
        if(!user) {
            res.status(400).send({ error: 'User does not exist'});
            return;
        }

        if(+user.balance < +req.body.bet_amount) {
            return res.status(400).send({ error: 'Not enough balance' });
        }

        const [event] = await EventRepository.getById(req.body.event_id);
        if(!event) {
            return res.status(404).send({ error: 'Event not found' });
        }

        const [odds] = await OddsRepository.getById(event.odds_id);
        if(!odds) {
            return res.status(404).send({ error: 'Odds not found' });
        }

        let multiplier;
        switch(req.body.prediction) {
            case 'w1':
                multiplier = odds.home_win;
                break;
            case 'w2':
                multiplier = odds.away_win;
                break;
            case 'x':
                multiplier = odds.draw;
                break;
        }
        const bet = await BetRepository.insert({
            ...req.body,
            multiplier,
            event_id: event.id
        });
        const currentBalance = user.balance - req.body.bet_amount;
        await UserRepository.update({
            balance: currentBalance,
        }, userId);
        statEmitter.emit('newBet');
        const betKeys = ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at', 'user_id'];
        rewriteKeysHelper(bet, betKeys);
        return res.send({
            ...bet,
            currentBalance: currentBalance,
        });
    }
}

module.exports = new BetsService();