const {UserRepository, TransactionRepository} = require("../repositories/repositories");
const {rewriteKeysHelper} = require("../helpers/helpers");

class TransactionService {
    async post(req, res) {
        const [user] = await UserRepository.getById(req.body.userId);
        if(!user) {
            res.status(400).send({ error: 'User does not exist'});
        }

        req.body.card_number = req.body.cardNumber;
        delete req.body.cardNumber;
        req.body.user_id = req.body.userId;
        delete req.body.userId;

        const result = await TransactionRepository.insert(req.body);
        const currentBalance = req.body.amount + user.balance;
        await UserRepository.updateField('balance', currentBalance, req.body.user_id);
        const resultKeys = ['user_id', 'card_number', 'created_at', 'updated_at'];
        rewriteKeysHelper(result, resultKeys);
        return res.send({
            ...result,
            currentBalance,
        });
    }
}

module.exports = new TransactionService();