const {statEmitter} = require("../config/stat");
const {getAccessToken} = require("../helpers/helpers");
const {UserRepository} = require("../repositories/repositories");

class UserService {
    async post(req, res) {
        try {
            req.body.balance = 0;
            const result = await UserRepository.insert(req.body)
            result.createdAt = result.created_at;
            delete result.created_at;
            result.updatedAt = result.updated_at;
            delete result.updated_at;
            statEmitter.emit('newUser');

            return res.send({
                ...result,
                accessToken: getAccessToken(result)
            });
        } catch (err) {
            if(err.code === '23505') {
                res.status(400).send({
                    error: err.detail
                });
            }
        }
    }

    async get (req, res) {
        const [result] = await UserRepository.getById(req.params.id);

        if(!result) {
            res.status(404).send({ error: 'User not found'});
        }
        res.send({
            ...result,
        });
    }

    async put(req, res) {
        try {
            const result = await UserRepository.update(req.body, req.params.id);
            res.send({
                ...result,
            });
        } catch (err) {
            if (err.code === '23505') {
                res.status(400).send({
                    error: err.detail
                });
            }
        }
    }
}

module.exports = new UserService();