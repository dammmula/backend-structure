const express = require("express");
const routes = require('./api/routes');
const errorHandlerMiddleware = require("./api/middlewares/error-handler.middleware");
const {addEventHandlers} = require("./config/stat");

const app = express();
const port = 3000;

app.use(express.json());
routes(app);
app.use(errorHandlerMiddleware);

app.listen(port, () => {
  addEventHandlers();
  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };